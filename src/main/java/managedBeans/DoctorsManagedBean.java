package managedBeans;

import model.DAO;
import model.entity.DoctorEntity;
import model.impl.DAOimpl;
import org.primefaces.PrimeFaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class DoctorsManagedBean {
    private DAO dao;

    List<DoctorEntity> doctors;
    DoctorEntity current;
    int prescriptionsOfDoctor;

    public DoctorsManagedBean() {
        this.dao = new DAOimpl();
        this.doctors = dao.doctors();
    }

    public List<DoctorEntity> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<DoctorEntity> doctors) {
        this.doctors = doctors;
    }

    public DoctorEntity getCurrent() {
        return current;
    }

    public void setCurrent(DoctorEntity current) {
        this.current = current;
    }

    public void acceptEditDoctor() {
        dao.editDoctor(current);
        this.doctors = dao.doctors();
        PrimeFaces.current().executeScript("PF('editDialog').hide()");
    }

    public void addNewDoctor() {
        this.current = new DoctorEntity();
    }

    public void acceptNewDoctor() {
        dao.createDoctor(current);
        this.doctors = dao.doctors();
        PrimeFaces.current().executeScript("PF('addDialog').hide()");
    }

    public void removeDoctor(DoctorEntity remove) {
        dao.deleteDoctor(remove);
        this.doctors = dao.doctors();
    }

    public String goToPatients() {
        return "patients";
    }

    public String goToPrescriptions() {
        return "prescriptions";
    }

    public void info(DoctorEntity doctor) {
        this.prescriptionsOfDoctor = dao.presriptionsOfDoctor(doctor);
    }

    public int getPrescriptionsOfDoctor() {
        return prescriptionsOfDoctor;
    }

    public void setPrescriptionsOfDoctor(int prescriptionsOfDoctor) {
        this.prescriptionsOfDoctor = prescriptionsOfDoctor;
    }
}
