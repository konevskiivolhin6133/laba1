package managedBeans;

import model.DAO;
import model.entity.PrescriptionEntity;
import model.impl.DAOimpl;
import org.primefaces.PrimeFaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class PrescriptionsManagedBean {
    private DAO dao;

    private List<PrescriptionEntity> prescriptions;

    private PrescriptionEntity current;
    private PrescriptionEntity search;

    public PrescriptionsManagedBean() {
        dao = new DAOimpl();
        this.prescriptions = dao.prescriptions();
        this.search = new PrescriptionEntity();
    }

    public List<PrescriptionEntity> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<PrescriptionEntity> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public PrescriptionEntity getCurrent() {
        return current;
    }

    public void setCurrent(PrescriptionEntity current) {
        this.current = current;
    }

    public void acceptEditPrescriptions() {
        dao.editPrescription(current);
        this.prescriptions = dao.prescriptions();
        PrimeFaces.current().executeScript("PF('editDialog').hide()");
    }

    public void addPrescriptions() {
        this.current = new PrescriptionEntity();
    }

    public void acceptAddPrescriptions() {
        dao.createPrescription(current);
        this.prescriptions = dao.prescriptions();
        PrimeFaces.current().executeScript("PF('addDialog').hide()");
    }

    public void removePrescriptions(PrescriptionEntity prescription) {
        dao.deletePrescription(prescription);
        this.prescriptions = dao.prescriptions();
    }

    public void searchPrescriptions() {
        this.prescriptions = dao.prescriptions();
        this.prescriptions = dao.filtered(search, prescriptions);
    }

    public void defaultPrescriptions() {
        this.prescriptions = dao.prescriptions();
    }

    public PrescriptionEntity getSearch() {
        return search;
    }

    public void setSearch(PrescriptionEntity search) {
        this.search = search;
    }

    public String goToDoctors() {
        return "doctors";
    }

    public String goToPatients() {
        return "patients";
    }
}
