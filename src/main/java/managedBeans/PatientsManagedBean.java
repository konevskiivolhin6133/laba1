package managedBeans;

import model.DAO;
import model.entity.PatientEntity;
import model.impl.DAOimpl;
import org.primefaces.PrimeFaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class PatientsManagedBean {
    private DAO dao;

    private List<PatientEntity> patients;

    private PatientEntity current;

    public PatientsManagedBean() {
        dao = new DAOimpl();
        this.patients = dao.patients();
    }

    public List<PatientEntity> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientEntity> patients) {
        this.patients = patients;
    }

    public PatientEntity getCurrent() {
        return current;
    }

    public void setCurrent(PatientEntity current) {
        this.current = current;
    }

    public void acceptEditPatient() {
        dao.editPatient(current);
        this.patients = dao.patients();
        PrimeFaces.current().executeScript("PF('editDialog').hide()");
    }

    public void removePatient(PatientEntity remove) {
        dao.deletePatient(remove);
        this.patients = dao.patients();
    }

    public void addNewPatient() {
        this.current = new PatientEntity();
    }

    public void acceptNewPatient() {
        dao.createPatient(current);
        this.patients = dao.patients();
        PrimeFaces.current().executeScript("PF('addDialog').hide()");
    }

    public String goToDoctors() {
        return "doctors";
    }

    public String goToPrescriptions() {
        return "prescriptions";
    }

}
