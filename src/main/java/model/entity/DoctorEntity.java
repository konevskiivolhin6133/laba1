package model.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "doctor", schema = "laba1ee", catalog = "")
public class DoctorEntity {
    private Integer id;
    private String firstName;
    private String fatherName;
    private String lastName;
    private String specification;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "fatherName")
    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "specification")
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorEntity that = (DoctorEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(fatherName, that.fatherName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(specification, that.specification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, fatherName, lastName, specification);
    }
}
