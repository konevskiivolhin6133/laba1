package model.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "prescription", schema = "laba1ee", catalog = "")
public class PrescriptionEntity {
    private Integer id;
    private Timestamp dateTime;
    private String description;
    private String priority;
    private DoctorEntity doctorByIdDoctor;
    private PatientEntity patientByIdPatient;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dateTime")
    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "priority")
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescriptionEntity that = (PrescriptionEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(description, that.description) &&
                Objects.equals(priority, that.priority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, description, priority);
    }

    @ManyToOne
    @JoinColumn(name = "idDoctor", referencedColumnName = "id", nullable = false)
    public DoctorEntity getDoctorByIdDoctor() {
        return doctorByIdDoctor;
    }

    public void setDoctorByIdDoctor(DoctorEntity doctorByIdDoctor) {
        this.doctorByIdDoctor = doctorByIdDoctor;
    }

    @ManyToOne
    @JoinColumn(name = "idPatient", referencedColumnName = "id", nullable = false)
    public PatientEntity getPatientByIdPatient() {
        return patientByIdPatient;
    }

    public void setPatientByIdPatient(PatientEntity patientByIdPatient) {
        this.patientByIdPatient = patientByIdPatient;
    }
}
