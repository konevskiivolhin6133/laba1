package model.impl;

import model.DAO;
import model.entity.DoctorEntity;
import model.entity.PatientEntity;
import model.entity.PrescriptionEntity;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DAOimpl implements DAO {

    private static final EntityManager entityManager = Persistence.createEntityManagerFactory("laba1ee").createEntityManager();

    @Transactional
    @Override
    public List<PatientEntity> patients() {
        return entityManager.createQuery("SELECT patient FROM PatientEntity patient").getResultList();
    }

    @Transactional
    @Override
    public boolean createPatient(PatientEntity patientEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(patientEntity);
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean editPatient(PatientEntity patientEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("UPDATE PatientEntity p SET p.fatherName = :fatherName, p.firstName = :firstName, p.lastName = :lastName, p.phone = :phone "
                + "WHERE p.id = :id");
        query.setParameter("id", patientEntity.getId());
        query.setParameter("fatherName", patientEntity.getFatherName());
        query.setParameter("firstName", patientEntity.getFirstName());
        query.setParameter("lastName", patientEntity.getLastName());
        query.setParameter("phone", patientEntity.getPhone());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean deletePatient(PatientEntity patientEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM PatientEntity p WHERE p.id = :idP");
        query.setParameter("idP", patientEntity.getId());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public List<DoctorEntity> doctors() {
        return entityManager.createQuery("SELECT doctor FROM DoctorEntity doctor").getResultList();
    }

    @Transactional
    @Override
    public boolean createDoctor(DoctorEntity doctorEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(doctorEntity);
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean editDoctor(DoctorEntity doctorEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("UPDATE DoctorEntity d SET d.fatherName = :fatherName, d.firstName = :firstName, d.lastName = :lastName, d.specification = :specification "
                + "WHERE d.id = :id");
        query.setParameter("id", doctorEntity.getId());
        query.setParameter("fatherName", doctorEntity.getFatherName());
        query.setParameter("firstName", doctorEntity.getFirstName());
        query.setParameter("lastName", doctorEntity.getLastName());
        query.setParameter("specification", doctorEntity.getSpecification());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean deleteDoctor(DoctorEntity doctorEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM DoctorEntity d WHERE d.id = :idD");
        query.setParameter("idD", doctorEntity.getId());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public int presriptionsOfDoctor(DoctorEntity doctor) {
        return 0;
    }

    @Transactional
    @Override
    public List<PrescriptionEntity> prescriptions() {
        return entityManager.createQuery("SELECT prescriptions FROM PrescriptionEntity prescriptions").getResultList();
    }

    @Transactional
    @Override
    public boolean createPrescription(PrescriptionEntity prescriptionEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(prescriptionEntity);
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean editPrescription(PrescriptionEntity prescriptionEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("UPDATE PrescriptionEntity p SET p.dateTime = :dateTime, p.description = :description, p.priority = :priority, p.doctorByIdDoctor = :doctorByIdDoctor, p.patientByIdPatient = :patientByIdPatient "
                + "WHERE p.id = :id");
        query.setParameter("id", prescriptionEntity.getId());
        query.setParameter("dateTime", prescriptionEntity.getDateTime());
        query.setParameter("description", prescriptionEntity.getDescription());
        query.setParameter("priority", prescriptionEntity.getPriority());
        query.setParameter("doctorByIdDoctor", prescriptionEntity.getDoctorByIdDoctor());
        query.setParameter("patientByIdPatient", prescriptionEntity.getDoctorByIdDoctor());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean deletePrescription(PrescriptionEntity prescriptionEntity) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM PrescriptionEntity p WHERE p.id = :idP");
        query.setParameter("idP", prescriptionEntity.getId());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public List<PrescriptionEntity> filtered(PrescriptionEntity search, List<PrescriptionEntity> data) {
        List<PrescriptionEntity> list = new ArrayList<>();
        return data;
    }
}
