package model;

import model.entity.DoctorEntity;
import model.entity.PatientEntity;
import model.entity.PrescriptionEntity;

import java.util.List;

public interface DAO {

    List<PatientEntity> patients();

    boolean createPatient(PatientEntity patientEntity);

    boolean editPatient(PatientEntity patientEntitye);

    boolean deletePatient(PatientEntity patientEntity);

    List<DoctorEntity> doctors();

    boolean createDoctor(DoctorEntity doctorEntity);

    boolean editDoctor(DoctorEntity doctorEntity);

    boolean deleteDoctor(DoctorEntity doctorEntity);

    int presriptionsOfDoctor(DoctorEntity doctorEntity);

    List<PrescriptionEntity> prescriptions();

    boolean createPrescription(PrescriptionEntity prescriptionEntity);

    boolean editPrescription(PrescriptionEntity prescriptionEntity);

    boolean deletePrescription(PrescriptionEntity prescriptionEntity);

    List<PrescriptionEntity> filtered(PrescriptionEntity search, List<PrescriptionEntity> data);
}
